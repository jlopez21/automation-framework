const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(true);

exports.config = {
  tests: '../src/tests/glance_account_tests/*.js',
  output: '../output',
  helpers: {
    Playwright: {
      url: process.env.URL || 'https://dw2.myglance.org',
      show: false,
      browser: process.env.Browser || 'chromium'
    }
  },
  include: {
    I: '../src/support/steps_file.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'automation-framework',
  plugins: {
    screenshotOnFail: {
      enabled: true
    },
    allure: {
      enable: true
    }
  }
}