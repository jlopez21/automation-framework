const compileTests = require('./lib/compileTests');

const optionDefinitions = [
    { name: 'compile-tests', alias: 'c', type: Boolean },
    { name: 'help', alias: 'h', type: Boolean },
    { name: 'base', alias: 'b' },
    { name: 'directory'},
    { name: 'key', alias: 'k', defaultValue: "keysfoDI2PTFgyHi3" },
    { name: 'table', alias: 't'}
]

const commandLineArgs = require('command-line-args')
const options = commandLineArgs(optionDefinitions)

if (options['compile-tests']) {
    compileTests(options.base, options.directory);
}