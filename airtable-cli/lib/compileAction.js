/**
 * 
 * @param {string} action 
 * @param {Array} parameters 
 * @returns 
 */
function compileAction (action, parameters) {
    switch (action) {
        case "am on page":
            if (parameters.length === 1)
                return `I.amOnPage(${ parameters[0] });`;
            else
                throw new Error('Function "am on page" expects only one parameter');
        case "append field":
            if (parameters.length === 2)
                return `I.appendField(${ parameters[0] }, ${ parameters[1] });`;
            else
                throw new Error('Function "append field" expects two parameters');
        case "attach file":
            if (parameters.length === 2)
                return `I.attachFile(${ parameters[0] }, ${ parameters[1] });`;
            else
                throw new Error('Function "attach file" expects two parameters');
        case "check option":
            if (parameters.length === 1)
                return `I.checkOption(${ parameters[0] });`;
            else
                throw new Error('Function "check option" expects only one parameter');
        case "clear all cookies":
            if (parameters.length === 0)
                return "I.clearCookie();";
            else
                throw new Error('Function "clear all cookies" expects no parameters ');
        case "clear cookie":
            if (parameters.length === 1)
                return `I.checkOption(${ parameters[0] });`;
            else
                throw new Error('Function "check option" expects only one parameter');
        case "click":
            if (parameters.length === 1)
                return `I.click(${ parameters[0] });`;
            else
                throw new Error(`The action "click" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "parameter" : "parameters"} found.`);
        case "close current tab":
            return `I.closeCurrentTab();`;
        case "close other tabs":
            return `I.closeOtherTabs();`;
        case "don't see":
            if (parameters.length === 1)
                return `I.dontSee(${ parameters[0] });`;
            else
                throw new Error(`The action "don't see" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "don't see cookie":
            if (parameters.length === 1)
                return `I.dontSeeCookie(${ parameters[0] });`;
            else
                throw new Error(`The action "don't see cookie" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "don't see element":
            if (parameters.length === 1)
                return `I.dontSeeElement(${ parameters[0] });`;
            else
                throw new Error(`The action "don't see element" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "don't see in field":
            if (parameters.length === 2)
                return `I.dontSeeInField(${ parameters[0] }, ${ parameters[1] });`;
            else
                throw new Error(`Error: "don't see in field" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "don't see in title":
            if (parameters.length === 1)
                return `I.dontSeeInTitle(${ parameters[0] }, ${ parameters[1] });`;
            else
                throw new Error(`Error: "don't see in title" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "double click":
            if (parameters.length === 1)
                return `I.doubleClick(${ parameters[0] });`;
            else
                throw new Error(`Error: "double click" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "exit session":
            return '});';
        case "fill field":
            if (parameters.length === 2)
                return `I.fillField(${ parameters[0] }, ${ parameters[1] });`;
            else
                throw new Error(`Error: "fill field" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "grab text from":
            if (parameters.length === 2)
                return `let ${ parameters[1] } = await I.grabTextFrom(${ parameters[0] });`;
            else
                throw new Error(`The action "grab text from" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "grab title":
            if (parameters.length === 2)
                return `let ${ parameters[1] } = I.grabTitle(${ parameters[0] });`;
            else
                throw new Error(`The action "grab title" function requires two parameters, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "grab value from":
            if (parameters.length === 2)
                return `let ${ parameters[1] } = I.grabValueFrom(${ parameters[0] });`;
            else
                throw new Error(`The action "grab value from" function requires two parameters, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "new session":
            if (parameters.length === 1)
                return `session('${ parameters[0] }', async () => {`;
            else
                throw new Error(`Error: "new session" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "save screenshot":
            if (parameters.length === 1)
                return `I.saveScreenshot(${ parameters[0] });`;
            else
                throw new Error(`Error: "save screenshot" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "see":
            if (parameters.length === 1)
                return `I.see(${parameters[0]});`;
            else
                throw new Error(`The action "see" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "see cookie":
            if (parameters.length === 1)
                return `I.seeCookie(${ parameters[0] });`;
            else
                throw new Error(`Error: "see cookie" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "see current URL equals":
            if (parameters.length === 1)
                return `I.seeCurrentUrlEquals(${ parameters[0] });`;
            else
                throw new Error(`Error: "see current URL equals" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "see element":
            if (parameters.length === 1)
                return `I.seeElement(${ parameters[0] });`;
            else
                throw new Error(`The action "see element" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "see in current url":
            if (parameters.length === 1)
                return `I.seeElement(${ parameters[0] });`;
            else
                throw new Error(`Error: "see in current url" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "see in field":
            if (parameters.length === 1)
                return `I.seeInField(${ parameters[0] });`;
            else
                throw new Error(`Error: "see in field" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "see in title":
            if (parameters.length === 1)
                return `I.seeInTitle(${ parameters[0] });`;
            else
                throw new Error(`Error: "see in title" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "see number of elements":
            if (parameters.length === 1)
                return `I.seeNumberOfElements(${ parameters[0] }, ${ parameters[1] });`;
            else
                throw new Error(`Error: "see number of elements" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "see number of visible elements":
            if (parameters.length === 2)
                return `I.seeNumberOfVisibleElements(${ parameters[0] }, ${ parameters[1] });`;
            else
                throw new Error(`Error: "see number of visible elements" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "see text equals":
            if (parameters.length === 2)
                return `I.seeTextEquals(${ parameters[1] }, ${ parameters[0] });`;
            else
                throw new Error(`Error: "see text equals" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "see title equals":
            if (parameters.length === 1)
                return `I.seeTitleEquals(${ parameters[0] });`;
            else
                throw new Error(`Error: "see title equals" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "select option":
            if (parameters.length === 2)
                return `I.selectOption(${ parameters[0] }, ${ parameters[1] });`;
            else
                throw new Error(`Error: "select option" function requires two parameters, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "set cookie":
            if (parameters.length === 1)
                return `I.setCookie(${ parameters[0] });`;
            else
                throw new Error(`Error: "set cookie" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "switch to window":
            if (parameters.length === 1)
                return `const windows = await I.grabAllWindowHandles();\n\tI.switchToWindow(windows[${ parameters[0] - 1 }]);`;
            else
                throw new Error(`Error: "switch to window" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "uncheck option":
            if (parameters.length === 1)
                return `I.uncheckOption(${ parameters[0] });`;
            else
                throw new Error(`Error: "uncheck option" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "wait":
            if (parameters.length === 1)
                return `I.wait(${ parameters[0] });`;
            else
                throw new Error(`The action "wait" function requires one parameter, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "wait for clickable":
            if (parameters.length === 2)
                return `I.waitForClickable(${ parameters[0] }, ${ parameters[1] });`;
            else
                throw new Error(`Error: "wait for clickable" function requires two parameters, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "wait for element":
            if (parameters.length === 2)
                return `I.waitForElement(${ parameters[0] }, ${ parameters[1] });`;
            else
                throw new Error(`The action "wait for element" function requires two parameters, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "wait for enable":
            if (parameters.length === 2)
                return `I.waitForEnabled(${ parameters[0] }, ${ parameters[1] });`;
            else
                throw new Error(`Error: "wait for enable" function requires two parameters, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
        case "wait URL equals":
            if (parameters.length === 2)
                return `I.waitUrlEquals(${ parameters[0] }, ${ parameters[1] });`;
            else
                throw new Error(`Error: "wait URL equals" function requires two parameters, ${parameters.length} ${parameters.length === 1 ? "was" : "where"} found.`);
    }
};

module.exports = compileAction;