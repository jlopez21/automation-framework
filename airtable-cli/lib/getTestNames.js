const Airtable = require('airtable');

async function getTestNames(baseID) {
    Airtable.configure({
        endpointUrl: 'https://api.airtable.com',
        apiKey: 'keysfoDI2PTFgyHi3'
    });
    let base = Airtable.base(baseID);
    let testNames = []; 
    let records = await base('Test Names').select({
        view: 'Grid view'
    }).all();

    records.forEach((record) => {
        testNames.push(record.fields.Tests);
    });

    return testNames;
}

module.exports = getTestNames;