const Airtable = require('airtable');
const getTestNames = require('./getTestNames');

async function getTestRecords(baseID) {
    let testNames = await getTestNames(baseID);
    Airtable.configure({
        endpointUrl: 'https://api.airtable.com',
        apiKey: 'keysfoDI2PTFgyHi3'
    });
    let base = Airtable.base(baseID);
    let tests = [];
    
    for (testName of testNames) {
        let testObj = {testName: "", actions: []};
        let records = await base(testName).select({
            view: 'Grid view'
        }).all();
        let actions = [];
        
        for (record of records) {
            let action = (
                await base('Actions').find(record.fields.Actions[0])
            ).fields['Action Name'];
            let parameters = [];

            if (record.fields.hasOwnProperty('Value (from Page Data)'))
                parameters.push(record.fields['Value (from Page Data)'][0]);
            if (record.fields.hasOwnProperty('Value (from Test Data)'))
                parameters.push(record.fields['Value (from Test Data)'][0]);
            if (record.fields.hasOwnProperty('Constants and Variables'))
                parameters.push(record.fields['Constants and Variables']);

            actions.push({action, parameters});
        };
        testObj.testName = testName;
        testObj.actions = actions;
        tests.push(testObj);
    };
    return tests;
}

module.exports = getTestRecords;