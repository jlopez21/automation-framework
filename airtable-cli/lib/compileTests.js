const getTestRecords = require('./getTestRecords');
const compileAction = require('./compileAction');
const fs = require('fs');

async function compileTests (baseID, path) {
    let tests = await getTestRecords(baseID);

    for (let i = 0, len = tests.length; i < len; i++) {
        let testFile = "";
        
        testFile += `Feature('${ tests[i].testName }');\n\n`;
        testFile += `Scenario('${ tests[i].testName }', async  ({ I }) => {\n`;

        for (let j = 0, len1 = tests[i].actions.length; j < len1; j++) {
            testFile += `\t${compileAction(tests[i].actions[j].action, tests[i].actions[j].parameters)}\n`
        }

        testFile += "});";
        
        fs.writeFile(`../src/tests/${path}/${tests[i].testName}.js`, testFile,  function(err) {
            if(err) {
                return console.log(err);
            }
        });
    }
};

module.exports = compileTests;